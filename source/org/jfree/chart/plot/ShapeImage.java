package org.jfree.chart.plot;

import java.awt.Graphics2D;
import java.awt.Shape;
import java.awt.geom.Rectangle2D;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;

public class ShapeImage {
	/**
	 * The image
	 */
	private BufferedImage image;
	
	/**
	 * The bounds of the image
	 */
	private Rectangle2D bounds;
	
	/**
	 * Constructor
	 * 
	 * @param filename  The filename of the image of the shape
	 */
	public ShapeImage(String filename) {
		try {
			loadFromFile(filename);
		} catch (Exception e) {
			System.err.println(e.getMessage());
		}
	}
	
	/**
	 * Loads the image for the shape from a file
	 * 
	 * @param filename  The filename of the image
	 * @throws Exception 
	 */
	public void loadFromFile(String filename) throws Exception {
		image = null;
		bounds = new Rectangle2D.Double(0, 0, 0, 0);
		
		// try to read the image
		try {
			image = ImageIO.read(new File(filename));
			// if we read the image succesfully, update the bounds
			bounds = new Rectangle2D.Double(0, 0, image.getWidth(), image.getHeight());
		} catch(Exception e) {
			// an IO error occured...
			throw new Exception("Could not load image " + filename + " as shape");
		}
	}
	
	/**
	 * Gets the image of the shape
	 */
	public BufferedImage getImage() {
		return image;
	}
	
	/**
	 * Gets the bounds of the image shape
	 */
	public Rectangle2D getBounds() {
		return bounds;
	}
	
	/**
	 * Performs the translation to the shape image
	 * 
	 * @param xx  The x movement to make
	 * @param yy  The y movement to make
	 */
	public void translate(double xx, double yy) {
		bounds = new Rectangle2D.Double(xx, yy, image.getWidth(), image.getHeight());
	}
	
	/**
	 * Gets the entity hotspot of the shape image
	 * 
	 * @return The entity hotspot of the shape
	 */
	public Shape getHotspot() {
		return (Shape)bounds;
	}

	/**
	 * Draw the shape image
	 * 
	 * @param g2  The graphics device
	 */
	public void draw(Graphics2D g2) {
		g2.drawImage(image, null, (int)bounds.getX() - image.getWidth() / 2, (int)bounds.getY() - image.getHeight() / 2);
	}
}
