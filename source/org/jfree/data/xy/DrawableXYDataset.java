package org.jfree.data.xy;

/**
 * Extends the XYDataset with to support calling drawShape2() method
 * @author ianvermeulen
 *
 */
public interface DrawableXYDataset extends XYDataset {
	/**
	 * Gets the drawable item from the given series
	 * 
	 * @param series  The series to choose the item from
	 * @param item  The wanted item
	 * @return  The drawable item of the series
	 */
	public DrawableXYDataItem getItem(int series, int item);
}
