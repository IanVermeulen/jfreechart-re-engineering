package org.jfree.data.xy;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Paint;
import java.awt.Shape;
import java.awt.Stroke;
import java.awt.geom.Rectangle2D;
import java.awt.image.BufferedImage;

import org.jfree.chart.axis.ValueAxis;
import org.jfree.chart.entity.EntityCollection;
import org.jfree.chart.plot.CrosshairState;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.chart.plot.ShapeImage;
import org.jfree.chart.plot.XYPlot;
import org.jfree.chart.renderer.xy.XYLineAndShapeRenderer;
import org.jfree.chart.util.RectangleEdge;
import org.jfree.chart.util.ShapeUtilities;

public class DrawableXYDataItem {
	 /**
     * A flag that indicates whether or not the item is "selected".
     *
     * @since 1.2.0
     */
    protected boolean selected;
    
    /**
     * A flag determining whether the shape of the item should be filled
     */
    private boolean filled;
    
    /**
     * A flag determining whether the item is visible
     */
    private boolean visible;
    
    /**
     * A flag determining if the shape uses fill paint
     */
    private boolean useFillPaint;
    
    /**
     * A flag determining if the shape uses outline paint
     */
    private boolean useOutlinePaint;
    
    /**
     * A flag determining if the shape uses fill paint
     */
    private boolean drawOutlines;
    
    /**
     * A flags determing to draw a label
     */
    private boolean drawLabel;

    /**
     * The shape of the item
     */
    private Shape shape;
    
    /**
     * The shape of the selected item
     */
    private Shape shapeSelected;
    
    /**
     * The fill paint of the item
     */
    private Paint fillPaint;
    
    /**
     * The outline paint of the item
     */
    private Paint outlinePaint;
    
    /**
     * The selected outline paint
     */
    private Paint outlineSelectedPaint;
    
    /**
     * The outline stroke
     */
    private Stroke outlineStroke;
    
    /**
     * The paint of the item
     */
    private Paint paint;
    
    /**
     * The shape image of the item
     */
    private ShapeImage shapeImage;
    
    /**
     * A flag determining whether the image shall be drawn, if false the shape shall be drawn
     */
    private boolean drawShapeImage;
    
    /**
     * A static field determining the color of each following data item
     */
    private static Color defaultColor = Color.red;
    
    
    /**
     * Sets the default color for the following data items
     */
    public static void setDefaultColor(Color color) {
    	defaultColor = color;
    }
    
    /**
     * Constructor
     */
    public DrawableXYDataItem() {
    	visible = true;
      	shape = new Rectangle2D.Double(-4, -4, 8, 8);
      	filled = true;
      	drawShapeImage = false;
      	drawOutlines = false;
      	paint = defaultColor;
      	fillPaint = defaultColor;
    }
    
    /**
     * Set image of the shape
     * 
     * @param filename  The filename of the image to use as shape
     */
    public void setShapeImage(String filename) {
    	shapeImage = new ShapeImage(filename);
    	drawShapeImage = true;
    }
    
    /**
     * Returns <code>true</code> if the data item is selected, and
     * <code>false</code> otherwise.
     *
     * @return A boolean.
     *
     * @see #setSelected(boolean)
     *
     * @since 1.2.0
     */
    public boolean isSelected() {
        return this.selected;
    }

    /**
     * Sets the selection state for this item.
     *
     * @param selected  the new selection state.
     *
     * @see #isSelected()
     *
     * @since 1.2.0
     */
    public void setSelected(boolean selected) {
        this.selected = selected;
    }
    
    /**
     * Checks if the shapes are filled
     * 
     * @return True if the shape is filled, false if the shape is not filled
     */
    public boolean isFilled() {
    	return filled;
    }
    
    /**
     * Checks if the shape is visible
     * 
     * @return True if the shape, false if the shape is not filled
     */
    public boolean isVisible() {
		return visible;
	}
    
    /**
     * Checks if the shape uses fill paint
     * 
     * @return  True if the shape uses fill paint, false if it doesn't
     */
    public boolean shouldUseFillPaint() {
		return useFillPaint;
	}

    /**
     * Sets if the shape should use fill paint
     * 
     * @param useFillPaint  True if the item should use a fill paint, false if it shouldn't
     */
	public void setUseFillPaint(boolean useFillPaint) {
		this.useFillPaint = useFillPaint;
	}
	
    /**
     * Checks if the shape uses fill paint
     * 
     * @return  True if the shape uses fill paint, false if it doesn't
     */
    public boolean shouldUseOutlinePaint() {
		return useOutlinePaint;
	}

    /**
     * Sets if the shape should use outline paint
     * 
     * @param useOutlinePaint  True if the item should use a outline paint, false if it shouldn't
     */
	public void setUseOutlinePaint(boolean useOutlinePaint) {
		this.useOutlinePaint = useOutlinePaint;
	}
	
	/**
     * Checks if the shape has an outline that should be drawn
     * 
     * @return  True if the item outline should be drawn, false if it shouldn't
     */
    public boolean shouldDrawOutline() {
		return drawOutlines;
	}

    /**
     * Sets if the shape has an outline that should be drawn
     * 
     * @param useFillPaint  True if the item outline should be drawn, false if it shouldn't
     */
	public void setDrawOutline(boolean drawOutlines) {
		this.drawOutlines = drawOutlines;
	}

	/**
     * Sets if the shape is visible
     * 
     * @param visible  Flag determining whether the shape is visible
     */
	public void setVisible(boolean visible) {
		this.visible = visible;
	}

	/**
     * Sets if the shapes are filled
     * 
     * @param filled  Flag whether the shapes are filled
     */
    public void setFilled(boolean filled) {
    	this.filled = filled;
    }
    
    /**
     * Gets the shape of the item
     * 
     * @return  The shape of the item, or if the item is selected, returns the selected shape
     */
	public Shape getShape() {
		if(selected)
			return shapeSelected;
		
		return shape;
	}

	/**
	 * Sets the shape of the item
	 * 
	 * @param shape  The shape of the item
	 */
	public void setShape(Shape shape) {
		this.shape = shape;
		drawShapeImage = false;
	}

    /**
	 * Sets the selected shape of the item
	 * 
	 * @param shape  The selected shape of the item
	 */
	public void setShapeSelected(Shape shapeSelected) {
		this.shapeSelected = shapeSelected;
	}

	/**
     * Gets the paint of the item
     * 
	 * @return the paint of the item
	 */
	public Paint getPaint() {
		return paint;
	}

	/**
	 * Sets the paint of the item
	 * 
	 * @param paint  The paint to set
	 */
	public void setPaint(Paint paint) {
		this.paint = paint;
	}

	/**
	 * Gets the fill paint of the item
	 * @return  the fillPaint
	 */
	public Paint getFillPaint() {
		return fillPaint;
	}

	/**
	 * Sets the fill paint of the item
	 * 
	 * @param fillPaint  the fillPaint to set
	 */
	public void setFillPaint(Paint fillPaint) {
		this.fillPaint = fillPaint;
	}

	/**
	 * Checks if the image should be drawn instead of the shape
	 * 
	 * @return  True if the image should be drawn, false if the shape should be drawn
	 */
	public boolean shouldDrawShapeImage() {
		return drawShapeImage;
	}

	/**
	 * Checks the outline paint of the item
	 * 
	 * @return  The outline paint of the item, the selected one if the item is selected
	 */
	public Paint getOutlinePaint() {
		if(isSelected())
			return outlineSelectedPaint;
		
		return outlinePaint;
	}

	/**
	 * Sets the outline paint of the item
	 * 
	 * @param outlinePaint  The outline paint of the item to set
	 */
	public void setOutlinePaint(Paint outlinePaint) {
		this.outlinePaint = outlinePaint;
	}

	/**
	 * Sets the selected outline paint of the selected item
	 * 
	 * @param outlineSelectedPaint  The selected outline paint of the item to set
	 */
	public void setOutlineSelectedPaint(Paint outlineSelectedPaint) {
		this.outlineSelectedPaint = outlineSelectedPaint;
	}

	/**
	 * Gets the outline stroke of the item
	 * 
	 * @return  The outline stroke of the item
	 */
	public Stroke getOutlineStroke() {
		return outlineStroke;
	}

	/**
	 * Sets the outline stroke of the item
	 * 
	 * @param outlineStroke  The outline stroke of the item to set
	 */
	public void setOutlineStroke(Stroke outlineStroke) {
		this.outlineStroke = outlineStroke;
	}

	/**
	 * Checks if the label should be drawn
	 * 
	 * @return  True if the label should be drawn, false if the label shouldn't be drawn
	 */
	public boolean shouldDrawLabel() {
		return drawLabel;
	}

	/**
	 * Sets if the label should be drawn
	 * 
	 * @param drawLabel  If the label should be drawn
	 */
	public void setDrawLabel(boolean drawLabel) {
		this.drawLabel = drawLabel;
	}

	/**
     * Draws the item shapes and adds chart entities (second pass). This method
     * draws the shapes which mark the item positions. If <code>entities</code>
     * is not <code>null</code> it will be populated with entity information
     * for points that fall within the data area.
     *
     * @param g2  the graphics device.
     * @param plot  the plot (can be used to obtain standard color
     *              information etc).
     * @param domainAxis  the domain axis.
     * @param dataArea  the area within which the data is being drawn.
     * @param rangeAxis  the range axis.
     * @param dataset  the dataset.
     * @param pass  the pass.
     * @param series  the series index (zero-based).
     * @param item  the item index (zero-based).
     * @param selected  is the data item selected?
     * @param crosshairState  the crosshair state.
     * @param entities the entity collection.
     */
    public void drawShape2(Graphics2D g2, Rectangle2D dataArea,
            XYPlot plot, XYDataset dataset, int pass, int series, int item,
            boolean selected, ValueAxis domainAxis, ValueAxis rangeAxis,
            CrosshairState crosshairState, EntityCollection entities, XYLineAndShapeRenderer renderer) {

        Shape entityArea = null;

        // get the data point...
        double x1 = dataset.getXValue(series, item);
        double y1 = dataset.getYValue(series, item);

        if (Double.isNaN(y1) || Double.isNaN(x1)) {
            return;
        }

        PlotOrientation orientation = plot.getOrientation();
        RectangleEdge xAxisLocation = plot.getDomainAxisEdge();
        RectangleEdge yAxisLocation = plot.getRangeAxisEdge();
        double transX1 = domainAxis.valueToJava2D(x1, dataArea, xAxisLocation);
        double transY1 = rangeAxis.valueToJava2D(y1, dataArea, yAxisLocation);

        if (isVisible()) {
        	if(shouldDrawShapeImage()) {
        		entityArea = drawShapeImage(g2, dataArea, orientation, transX1, transY1);
        	}
        	else {
        		entityArea = drawShape(g2, dataArea, orientation, transX1,
						transY1);
        	}
            
        }

        double xx = transX1;
        double yy = transY1;
        if (orientation == PlotOrientation.HORIZONTAL) {
            xx = transY1;
            yy = transX1;
        }

        // draw the item label if there is one...
        if (shouldDrawLabel()) {
            renderer.drawItemLabel(g2, orientation, dataset, series, item, selected, 
                    xx, yy, (y1 < 0.0));
        }

        int domainAxisIndex = plot.getDomainAxisIndex(domainAxis);
        int rangeAxisIndex = plot.getRangeAxisIndex(rangeAxis);
        renderer.updateCrosshairValues(crosshairState, x1, y1, domainAxisIndex,
                rangeAxisIndex, transX1, transY1, orientation);

        // add an entity for the item, but only if it falls within the data
        // area...
        if (entities != null 
                && ShapeUtilities.isPointInRect(xx, yy, dataArea)) {
            renderer.addEntity(entities, entityArea, dataset, series, item, selected,
                    xx, yy);
        }
    }

	/**
	 * Draws the shape
	 * 
	 * @param g2  The graphics device
	 * @param dataArea  The data area in which to draw
	 * @param orientation  The orientation of the plot
	 * @param transX1  The translation on the x side
	 * @param transY1 The translation on the y side
	 * 
	 * @return  The entity area
	 */
	private Shape drawShape(Graphics2D g2, Rectangle2D dataArea,
			PlotOrientation orientation, double transX1, double transY1) {
		Shape entityArea;
		Shape shape = getShape();
		if (orientation == PlotOrientation.HORIZONTAL) {
		    shape = ShapeUtilities.createTranslatedShape(shape, transY1,
		            transX1);
		}
		else if (orientation == PlotOrientation.VERTICAL) {
		    shape = ShapeUtilities.createTranslatedShape(shape, transX1,
		            transY1);
		}
		
		entityArea = shape;
		if (shape.intersects(dataArea)) {
		    if (isFilled()) {
		        if (shouldUseFillPaint()) {
		            g2.setPaint(getFillPaint());
		        }
		        else {
		            g2.setPaint(getPaint());
		        }
		        g2.fill(shape);
		    }
		    if (shouldDrawOutline()) {
		        if (shouldUseOutlinePaint()) {
		            g2.setPaint(getOutlinePaint());
		        }
		        else {
		            g2.setPaint(getPaint());
		        }
		        g2.setStroke(getOutlineStroke());
		        g2.draw(shape);
		    }
		}
		return entityArea;
	}
	
	/**
	 * Draws the shape image
	 * 
	 * @param g2  The graphics device
	 * @param dataArea  The data area in which to draw
	 * @param orientation  The orientation of the plot
	 * @param transX1  The translation on the x side
	 * @param transY1 The translation on the y side
	 * 
	 * @return  The entity area
	 */
	private Shape drawShapeImage(Graphics2D g2, Rectangle2D dataArea,
			PlotOrientation orientation, double transX1, double transY1) {
		if (orientation == PlotOrientation.HORIZONTAL)
		    shapeImage.translate(transY1, transX1);
		else if (orientation == PlotOrientation.VERTICAL)
		    shapeImage.translate(transX1, transY1);
		
		if(shapeImage.getHotspot().intersects(dataArea))
			shapeImage.draw(g2);
		
		return shapeImage.getHotspot();
	}
    
}
