/* ===========================================================
 * JFreeChart : a free chart library for the Java(tm) platform
 * ===========================================================
 *
 * (C) Copyright 2000-2009, by Object Refinery Limited and Contributors.
 *
 * Project Info:  http://www.jfree.org/jfreechart/index.html
 *
 * This library is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or
 * (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
 * License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301,
 * USA.
 *
 * [Java is a trademark or registered trademark of Sun Microsystems, Inc.
 * in the United States and other countries.]
 *
 * --------------------------------
 * XYLineAndShapeRendererTests.java
 * --------------------------------
 * (C) Copyright 2004-2008, by Object Refinery Limited and Contributors.
 *
 * Original Author:  David Gilbert (for Object Refinery Limited);
 * Contributor(s):   -;
 *
 * Changes
 * -------
 * 27-Jan-2004 : Version 1 (DG);
 * 07-Jan-2005 : Added check for findRangeBounds() method (DG);
 * 21-Feb-2007 : Check independence in testCloning() (DG);
 * 17-May-2007 : Added testGetLegendItemSeriesIndex() (DG);
 * 22-Apr-2008 : Added testPublicCloneable() (DG);
 *
 */

package org.jfree.chart.renderer.xy.junit;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.geom.GeneralPath;
import java.awt.geom.Line2D;
import java.awt.geom.Rectangle2D;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.ObjectInput;
import java.io.ObjectInputStream;
import java.io.ObjectOutput;
import java.io.ObjectOutputStream;

import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;

import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartRenderingInfo;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.LegendItem;
import org.jfree.chart.axis.NumberAxis;
import org.jfree.chart.axis.ValueAxis;
import org.jfree.chart.junit.TestUtilities;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.chart.plot.PlotRenderingInfo;
import org.jfree.chart.plot.XYPlot;
import org.jfree.chart.renderer.xy.StandardXYItemRenderer;
import org.jfree.chart.renderer.xy.XYItemRendererState;
import org.jfree.chart.renderer.xy.XYLineAndShapeRenderer;
import org.jfree.chart.urls.TimeSeriesURLGenerator;
import org.jfree.chart.util.PublicCloneable;
import org.jfree.data.Range;
import org.jfree.data.time.Month;
import org.jfree.data.time.TimeSeries;
import org.jfree.data.time.TimeSeriesCollection;
import org.jfree.data.xy.TableXYDataset;
import org.jfree.data.xy.XYSeries;
import org.jfree.data.xy.XYSeriesCollection;

/**
 * Tests for the {@link XYLineAndShapeRenderer} class.
 */
public class XYLineAndShapeRendererTests extends TestCase {

    /**
     * Returns the tests as a test suite.
     *
     * @return The test suite.
     */
    public static Test suite() {
        return new TestSuite(XYLineAndShapeRendererTests.class);
    }

    /**
     * Constructs a new set of tests.
     *
     * @param name  the name of the tests.
     */
    public XYLineAndShapeRendererTests(String name) {
        super(name);
    }

    /**
     * Test that the equals() method distinguishes all fields.
     */
    public void testEquals() {

        XYLineAndShapeRenderer r1 = new XYLineAndShapeRenderer();
        XYLineAndShapeRenderer r2 = new XYLineAndShapeRenderer();
        assertEquals(r1, r2);
        assertEquals(r2, r1);

        r1.setSeriesLinesVisible(3, true);
        assertFalse(r1.equals(r2));
        r2.setSeriesLinesVisible(3, true);
        assertTrue(r1.equals(r2));

        r1.setBaseLinesVisible(false);
        assertFalse(r1.equals(r2));
        r2.setBaseLinesVisible(false);
        assertTrue(r1.equals(r2));

        r1.setLegendLine(new Line2D.Double(1.0, 2.0, 3.0, 4.0));
        assertFalse(r1.equals(r2));
        r2.setLegendLine(new Line2D.Double(1.0, 2.0, 3.0, 4.0));
        assertTrue(r1.equals(r2));

        r1.setSeriesShapesVisible(3, true);
        assertFalse(r1.equals(r2));
        r2.setSeriesShapesVisible(3, true);
        assertTrue(r1.equals(r2));

        r1.setBaseShapesVisible(false);
        assertFalse(r1.equals(r2));
        r2.setBaseShapesVisible(false);
        assertTrue(r1.equals(r2));

        r1.setSeriesShapesFilled(3, true);
        assertFalse(r1.equals(r2));
        r2.setSeriesShapesFilled(3, true);
        assertTrue(r1.equals(r2));

        r1.setBaseShapesFilled(false);
        assertFalse(r1.equals(r2));
        r2.setBaseShapesFilled(false);
        assertTrue(r1.equals(r2));

        r1.setDrawOutlines(!r1.getDrawOutlines());
        assertFalse(r1.equals(r2));
        r2.setDrawOutlines(r1.getDrawOutlines());
        assertTrue(r1.equals(r2));

        r1.setUseOutlinePaint(true);
        assertFalse(r1.equals(r2));
        r2.setUseOutlinePaint(true);
        assertTrue(r1.equals(r2));

        r1.setUseFillPaint(true);
        assertFalse(r1.equals(r2));
        r2.setUseFillPaint(true);
        assertTrue(r1.equals(r2));

        r1.setDrawSeriesLineAsPath(true);
        assertFalse(r1.equals(r2));
        r2.setDrawSeriesLineAsPath(true);
        assertTrue(r1.equals(r2));
    }

    /**
     * Test that the equals() method works for a TimeSeriesURLGenerator.
     */
    public void testEquals2() {
        XYLineAndShapeRenderer r1 = new XYLineAndShapeRenderer();
        XYLineAndShapeRenderer r2 = new XYLineAndShapeRenderer();
        assertEquals(r1, r2);
        assertEquals(r2, r1);

        r1.setBaseURLGenerator(new TimeSeriesURLGenerator());
        assertFalse(r1.equals(r2));
        r2.setBaseURLGenerator(new TimeSeriesURLGenerator());
        assertTrue(r1.equals(r2));
    }


    /**
     * Two objects that are equal are required to return the same hashCode.
     */
    public void testHashcode() {
        XYLineAndShapeRenderer r1 = new XYLineAndShapeRenderer();
        XYLineAndShapeRenderer r2 = new XYLineAndShapeRenderer();
        assertTrue(r1.equals(r2));
        int h1 = r1.hashCode();
        int h2 = r2.hashCode();
        assertEquals(h1, h2);
    }

    /**
     * Confirm that cloning works.
     */
    public void testCloning() {
        Rectangle2D legendShape = new Rectangle2D.Double(1.0, 2.0, 3.0, 4.0);
        XYLineAndShapeRenderer r1 = new XYLineAndShapeRenderer();
        r1.setLegendLine(legendShape);
        XYLineAndShapeRenderer r2 = null;
        try {
            r2 = (XYLineAndShapeRenderer) r1.clone();
        }
        catch (CloneNotSupportedException e) {
            e.printStackTrace();
        }
        assertTrue(r1 != r2);
        assertTrue(r1.getClass() == r2.getClass());
        assertTrue(r1.equals(r2));

        r1.setSeriesLinesVisible(0, false);
        assertFalse(r1.equals(r2));
        r2.setSeriesLinesVisible(0, false);
        assertTrue(r1.equals(r2));

        legendShape.setRect(4.0, 3.0, 2.0, 1.0);
        assertFalse(r1.equals(r2));
        r2.setLegendLine(new Rectangle2D.Double(4.0, 3.0, 2.0, 1.0));
        assertTrue(r1.equals(r2));

        r1.setSeriesShapesVisible(1, true);
        assertFalse(r1.equals(r2));
        r2.setSeriesShapesVisible(1, true);
        assertTrue(r1.equals(r2));

        r1.setSeriesShapesFilled(1, true);
        assertFalse(r1.equals(r2));
        r2.setSeriesShapesFilled(1, true);
        assertTrue(r1.equals(r2));
    }

    /**
     * Verify that this class implements {@link PublicCloneable}.
     */
    public void testPublicCloneable() {
        XYLineAndShapeRenderer r1 = new XYLineAndShapeRenderer();
        assertTrue(r1 instanceof PublicCloneable);
    }

    /**
     * Serialize an instance, restore it, and check for equality.
     */
    public void testSerialization() {

        XYLineAndShapeRenderer r1 = new XYLineAndShapeRenderer();
        XYLineAndShapeRenderer r2 = null;
        try {
            ByteArrayOutputStream buffer = new ByteArrayOutputStream();
            ObjectOutput out = new ObjectOutputStream(buffer);
            out.writeObject(r1);
            out.close();

            ObjectInput in = new ObjectInputStream(
                    new ByteArrayInputStream(buffer.toByteArray()));
            r2 = (XYLineAndShapeRenderer) in.readObject();
            in.close();
        }
        catch (Exception e) {
            e.printStackTrace();
        }
        assertEquals(r1, r2);

    }

    /**
     * Check that the renderer is calculating the domain bounds correctly.
     */
    public void testFindDomainBounds() {
        XYSeriesCollection dataset
                = RendererXYPackageTests.createTestXYSeriesCollection();
        JFreeChart chart = ChartFactory.createXYLineChart(
                "Test Chart", "X", "Y", dataset, false);
        XYPlot plot = (XYPlot) chart.getPlot();
        NumberAxis domainAxis = (NumberAxis) plot.getDomainAxis();
        domainAxis.setAutoRangeIncludesZero(false);
        Range bounds = domainAxis.getRange();
        assertFalse(bounds.contains(0.9));
        assertTrue(bounds.contains(1.0));
        assertTrue(bounds.contains(2.0));
        assertFalse(bounds.contains(2.10));
    }
    
    /**
     * Checks if renderer draws the item correctly
     */
    public void testDrawItem() {
    	BufferedImage bufferedImage = new BufferedImage(200, 200, BufferedImage.TYPE_INT_RGB);
    	Graphics2D g2 = bufferedImage.createGraphics();
    	XYSeriesCollection dataset = RendererXYPackageTests.createTestXYSeriesCollection();
    	JFreeChart chart = ChartFactory.createXYLineChart("Test Chart", "X", "Y", dataset, false);
    	XYPlot plot = (XYPlot) chart.getPlot();
    	XYLineAndShapeRenderer renderer = (XYLineAndShapeRenderer)plot.getRenderer();
    	Rectangle2D dataArea = new Rectangle2D.Double(0, 0, 200, 200);
    	XYItemRendererState state = 
    			renderer.initialise(g2, dataArea, plot, dataset, null);
    	
    	// check if nothing was written when series is not visible  	
    	BufferedImage cloneBufferedImage = new BufferedImage(200, 200, BufferedImage.TYPE_INT_RGB);
    	
    	renderer.setSeriesVisible(0, false);
    	renderer.drawItem(g2, state, dataArea, plot, plot.getDomainAxis(), plot.getRangeAxis(), dataset, 0, 1, false, 0);
    
    	// no changes were made to the image
    	assertTrue(TestUtilities.bufferedImagesEqual(bufferedImage, cloneBufferedImage));
    	
    	state = renderer.initialise(g2, dataArea, plot, dataset, null);
    	renderer.setSeriesVisible(0, true);
    	renderer.drawItem(g2, state, dataArea, plot, plot.getDomainAxis(), plot.getRangeAxis(), dataset, 0, 1, false, 0);
    	
    	// make sure that changes were made to the image
    	assertFalse(TestUtilities.bufferedImagesEqual(bufferedImage, cloneBufferedImage));
    	
    	bufferedImage = new BufferedImage(200, 200, BufferedImage.TYPE_INT_RGB);
    	cloneBufferedImage = new BufferedImage(200, 200, BufferedImage.TYPE_INT_RGB);
    	g2 = bufferedImage.createGraphics();
    	state = renderer.initialise(g2, dataArea, plot, dataset, null);
    	
    	// create a new state with a series path (manually chosen)
    	XYLineAndShapeRenderer.State s = new XYLineAndShapeRenderer.State(null);
    	s.seriesPath = new GeneralPath(new Line2D.Double(0, 0, 50, 50));
    	
    	renderer.setSeriesLinesVisible(0, true);
    	renderer.setDrawSeriesLineAsPath(true);
    	
    	// make sure the item is drawn
    	renderer.drawItem(g2, s, dataArea, plot, plot.getDomainAxis(), plot.getRangeAxis(), dataset, 0, state.getLastItemIndex(), false, 0);

    	assertFalse(TestUtilities.bufferedImagesEqual(bufferedImage, cloneBufferedImage));
    	
    	bufferedImage = new BufferedImage(200, 200, BufferedImage.TYPE_INT_RGB);
    	cloneBufferedImage = new BufferedImage(200, 200, BufferedImage.TYPE_INT_RGB);
    	g2 = bufferedImage.createGraphics();
    	
    	state = renderer.initialise(g2, dataArea, plot, dataset, null);
    	renderer.setSeriesLinesVisible(0, false);
    	renderer.drawItem(g2, state, dataArea, plot, plot.getDomainAxis(), plot.getRangeAxis(), dataset, 0, state.getLastItemIndex(), false, 0);
    	
    	assertTrue(TestUtilities.bufferedImagesEqual(bufferedImage, cloneBufferedImage));
    	
    	bufferedImage = new BufferedImage(200, 200, BufferedImage.TYPE_INT_RGB);
    	cloneBufferedImage = new BufferedImage(200, 200, BufferedImage.TYPE_INT_RGB);
    	g2 = bufferedImage.createGraphics();
    	
    	state = renderer.initialise(g2, dataArea, plot, dataset, null);
    	renderer.setSeriesLinesVisible(0, false);
    	renderer.drawItem(g2, state, dataArea, plot, plot.getDomainAxis(), plot.getRangeAxis(), dataset, 0, state.getLastItemIndex(), false, 3);
    	
    	assertTrue(TestUtilities.bufferedImagesEqual(bufferedImage, cloneBufferedImage));
    	
    	bufferedImage = new BufferedImage(200, 200, BufferedImage.TYPE_INT_RGB);
    	cloneBufferedImage = new BufferedImage(200, 200, BufferedImage.TYPE_INT_RGB);
    	g2 = bufferedImage.createGraphics();
    	
    	state = renderer.initialise(g2, dataArea, plot, dataset, new PlotRenderingInfo(new ChartRenderingInfo()));
    	
    	assertFalse(state.getInfo() == null);
    	
    	renderer.setSeriesLinesVisible(0, false);
    	renderer.drawItem(g2, state, dataArea, plot, plot.getDomainAxis(), plot.getRangeAxis(), dataset, 0, state.getLastItemIndex(), false, 1);

    	assertTrue(TestUtilities.bufferedImagesEqual(bufferedImage, cloneBufferedImage));
    }
    
    /**
     * Checks if drawing the nodes is done correctly
     */
    public void testDrawShape2NaN() {
    	BufferedImage bufferedImage = new BufferedImage(200, 200, BufferedImage.TYPE_INT_RGB);
    	Graphics2D g2 = bufferedImage.createGraphics();
    	XYSeriesCollection dataset = new XYSeriesCollection();
    	dataset.addSeries(new XYSeries("NaNTest"));
      	dataset.getSeries(0).add(Double.NaN, Double.NaN);
    	dataset.getSeries(0).fireSeriesChanged();
    	
    	JFreeChart chart = ChartFactory.createXYLineChart("Test Chart", "X", "Y", dataset, false);
    	XYPlot plot = (XYPlot) chart.getPlot();
    	XYLineAndShapeRenderer renderer = (XYLineAndShapeRenderer)plot.getRenderer();
    	Rectangle2D dataArea = new Rectangle2D.Double(0, 0, 200, 200);
    	
    	// check if nothing was written when series is not visible  	
    	BufferedImage cloneBufferedImage = new BufferedImage(200, 200, BufferedImage.TYPE_INT_RGB);
    	
    	renderer.drawShape2(g2, dataArea, plot, dataset, 0, 0, 0, false, plot.getDomainAxis(), plot.getRangeAxis(), null, null);
    	
    	// make sure nothing is written
    	assertTrue(TestUtilities.bufferedImagesEqual(bufferedImage, cloneBufferedImage));
    }
    
    /**
     * Checks if drawing the nodes is done correctly
     */
    public void testDrawShape2() {
    	BufferedImage bufferedImage = new BufferedImage(200, 200, BufferedImage.TYPE_INT_RGB);
    	Graphics2D g2 = bufferedImage.createGraphics();
    	XYSeriesCollection dataset = new XYSeriesCollection();
    	dataset.addSeries(new XYSeries("NaNTest"));
      	dataset.getSeries(0).add(1, 1);
    	dataset.getSeries(0).fireSeriesChanged();
  
    	
    	JFreeChart chart = ChartFactory.createXYLineChart("Test Chart", "X", "Y", dataset, false);
    	XYPlot plot = (XYPlot) chart.getPlot();
    	plot.setOrientation(PlotOrientation.HORIZONTAL);
    	XYLineAndShapeRenderer renderer = (XYLineAndShapeRenderer)plot.getRenderer();
    	renderer.setSeriesShapesVisible(0, true);
    	Rectangle2D dataArea = new Rectangle2D.Double(0, 0, 200, 200);
    	
    	BufferedImage cloneBufferedImage = new BufferedImage(200, 200, BufferedImage.TYPE_INT_RGB);
    	
    	renderer.drawShape2(g2, dataArea, plot, dataset, 0, 0, 0, false, plot.getDomainAxis(), plot.getRangeAxis(), null, null);
    	
    	// make sure something is written
    	assertFalse(TestUtilities.bufferedImagesEqual(bufferedImage, cloneBufferedImage));
    }
    
    /**
     * Checks if drawing the nodes is done correctly
     */
    public void testDrawShape2UseFillpaint() {
    	BufferedImage bufferedImage = new BufferedImage(200, 200, BufferedImage.TYPE_INT_RGB);
    	Graphics2D g2 = bufferedImage.createGraphics();
    	XYSeriesCollection dataset = new XYSeriesCollection();
    	dataset.addSeries(new XYSeries("NaNTest"));
      	dataset.getSeries(0).add(1, 1);
    	dataset.getSeries(0).fireSeriesChanged();
  
    	
    	JFreeChart chart = ChartFactory.createXYLineChart("Test Chart", "X", "Y", dataset, false);
    	XYPlot plot = (XYPlot) chart.getPlot();
    	plot.setOrientation(PlotOrientation.HORIZONTAL);
    	XYLineAndShapeRenderer renderer = (XYLineAndShapeRenderer)plot.getRenderer();
    	renderer.setSeriesShapesVisible(0, true);
    	Rectangle2D dataArea = new Rectangle2D.Double(0, 0, 200, 200);
    	
    	BufferedImage cloneBufferedImage = new BufferedImage(200, 200, BufferedImage.TYPE_INT_RGB);
    	
    	renderer.setUseFillPaint(true);
    	renderer.drawShape2(g2, dataArea, plot, dataset, 0, 0, 0, false, plot.getDomainAxis(), plot.getRangeAxis(), null, null);
    	
    	// make sure something is written
    	assertFalse(TestUtilities.bufferedImagesEqual(bufferedImage, cloneBufferedImage));
    }
    
    /**
     * Checks if drawing the nodes is done correctly
     */
    public void testDrawShape2UseOutlinePaint() {
    	BufferedImage bufferedImage = new BufferedImage(200, 200, BufferedImage.TYPE_INT_RGB);
    	Graphics2D g2 = bufferedImage.createGraphics();
    	XYSeriesCollection dataset = new XYSeriesCollection();
    	dataset.addSeries(new XYSeries("NaNTest"));
      	dataset.getSeries(0).add(1, 1);
    	dataset.getSeries(0).fireSeriesChanged();
  
    	
    	JFreeChart chart = ChartFactory.createXYLineChart("Test Chart", "X", "Y", dataset, false);
    	XYPlot plot = (XYPlot) chart.getPlot();
    	plot.setOrientation(PlotOrientation.HORIZONTAL);
    	XYLineAndShapeRenderer renderer = (XYLineAndShapeRenderer)plot.getRenderer();
    	renderer.setSeriesShapesVisible(0, true);
    	Rectangle2D dataArea = new Rectangle2D.Double(0, 0, 200, 200);
    	
    	BufferedImage cloneBufferedImage = new BufferedImage(200, 200, BufferedImage.TYPE_INT_RGB);
    	
    	renderer.setUseOutlinePaint(true);
    	renderer.drawShape2(g2, dataArea, plot, dataset, 0, 0, 0, false, plot.getDomainAxis(), plot.getRangeAxis(), null, null);
    	
    	// make sure something is written
    	assertFalse(TestUtilities.bufferedImagesEqual(bufferedImage, cloneBufferedImage));
    }
    
    /**
     * Checks if drawing the nodes is done correctly
     */
    public void testDrawShape2Label() {
    	BufferedImage bufferedImage = new BufferedImage(200, 200, BufferedImage.TYPE_INT_RGB);
    	Graphics2D g2 = bufferedImage.createGraphics();
    	XYSeriesCollection dataset = new XYSeriesCollection();
    	dataset.addSeries(new XYSeries("NaNTest"));
      	dataset.getSeries(0).add(1, 1);
    	dataset.getSeries(0).fireSeriesChanged();
  
    	
    	JFreeChart chart = ChartFactory.createXYLineChart("Test Chart", "X", "Y", dataset, false);
    	XYPlot plot = (XYPlot) chart.getPlot();
    	plot.setOrientation(PlotOrientation.HORIZONTAL);
    	XYLineAndShapeRenderer renderer = (XYLineAndShapeRenderer)plot.getRenderer();
    	renderer.setSeriesShapesVisible(0, true);
    	Rectangle2D dataArea = new Rectangle2D.Double(0, 0, 200, 200);
    	
    	BufferedImage cloneBufferedImage = new BufferedImage(200, 200, BufferedImage.TYPE_INT_RGB);
    	
    	renderer.setSeriesItemLabelsVisible(0, true);
    	renderer.drawShape2(g2, dataArea, plot, dataset, 0, 0, 0, false, plot.getDomainAxis(), plot.getRangeAxis(), null, null);
    	
    	// make sure something is written
    	assertFalse(TestUtilities.bufferedImagesEqual(bufferedImage, cloneBufferedImage));
    }
    
    /**
     * Checks if drawing the nodes is done correctly
     */
    public void testTimeSeriesDrawShape2() {
    	BufferedImage bufferedImage = new BufferedImage(200, 200, BufferedImage.TYPE_INT_RGB);
    	Graphics2D g2 = bufferedImage.createGraphics();
    	TimeSeriesCollection dataset = new TimeSeriesCollection();
    	dataset.addSeries(new TimeSeries("NaNTest"));
      	dataset.getSeries(0).add(new Month(2, 2001), 181.8);
    	dataset.getSeries(0).fireSeriesChanged();
  
    	
    	JFreeChart chart = ChartFactory.createTimeSeriesChart("Test Chart", "X", "Y", dataset, false);
    	XYPlot plot = (XYPlot) chart.getPlot();
    	plot.setOrientation(PlotOrientation.HORIZONTAL);
    	XYLineAndShapeRenderer renderer = (XYLineAndShapeRenderer)plot.getRenderer();
    	
    	
    	Rectangle2D dataArea = new Rectangle2D.Double(0, 0, 200, 200);
    	
    	BufferedImage cloneBufferedImage = new BufferedImage(200, 200, BufferedImage.TYPE_INT_RGB);
    	
    	renderer.drawItem(g2, new XYItemRendererState(new PlotRenderingInfo(new ChartRenderingInfo())), dataArea, plot, plot.getDomainAxis(), plot.getRangeAxis(), dataset, 0, 0, false, 1);
    	
    	// make sure something is written
    	assertFalse(TestUtilities.bufferedImagesEqual(bufferedImage, cloneBufferedImage));
    }
    
    /**
     * Checks if drawing the nodes is done correctly
     */
    public void testTimeSeriesDrawShape2Vertical() {
    	BufferedImage bufferedImage = new BufferedImage(200, 200, BufferedImage.TYPE_INT_RGB);
    	Graphics2D g2 = bufferedImage.createGraphics();
    	TimeSeriesCollection dataset = new TimeSeriesCollection();
    	dataset.addSeries(new TimeSeries("NaNTest"));
      	dataset.getSeries(0).add(new Month(2, 2001), 181.8);
    	dataset.getSeries(0).fireSeriesChanged();
  
    	
    	JFreeChart chart = ChartFactory.createTimeSeriesChart("Test Chart", "X", "Y", dataset, false);
    	XYPlot plot = (XYPlot) chart.getPlot();
    	plot.setOrientation(PlotOrientation.VERTICAL);
    	XYLineAndShapeRenderer renderer = (XYLineAndShapeRenderer)plot.getRenderer();
    	
    	
    	Rectangle2D dataArea = new Rectangle2D.Double(0, 0, 200, 200);
    	
    	BufferedImage cloneBufferedImage = new BufferedImage(200, 200, BufferedImage.TYPE_INT_RGB);
    	
    	renderer.drawItem(g2, new XYItemRendererState(new PlotRenderingInfo(new ChartRenderingInfo())), dataArea, plot, plot.getDomainAxis(), plot.getRangeAxis(), dataset, 0, 0, false, 1);
    	
    	// make sure something is written
    	assertFalse(TestUtilities.bufferedImagesEqual(bufferedImage, cloneBufferedImage));
    }
    
    /**
     * Checks if drawing the nodes is done correctly
     */
    public void testTimeSeriesDrawShapeImage() {
    	BufferedImage bufferedImage = new BufferedImage(200, 200, BufferedImage.TYPE_INT_RGB);
    	Graphics2D g2 = bufferedImage.createGraphics();
    	TimeSeriesCollection dataset = new TimeSeriesCollection();
    	dataset.addSeries(new TimeSeries("NaNTest"));
      	dataset.getSeries(0).add(new Month(2, 2001), 181.8);
      	dataset.getItem(0, 0).setShapeImage("resources/pokeball.png");
    	dataset.getSeries(0).fireSeriesChanged();
  
    	
    	JFreeChart chart = ChartFactory.createTimeSeriesChart("Test Chart", "X", "Y", dataset, false);
    	XYPlot plot = (XYPlot) chart.getPlot();
    	plot.setOrientation(PlotOrientation.HORIZONTAL);
    	XYLineAndShapeRenderer renderer = (XYLineAndShapeRenderer)plot.getRenderer();
    	
    	
    	Rectangle2D dataArea = new Rectangle2D.Double(0, 0, 200, 200);
    	
    	BufferedImage cloneBufferedImage = new BufferedImage(200, 200, BufferedImage.TYPE_INT_RGB);
    	
    	renderer.drawItem(g2, new XYItemRendererState(new PlotRenderingInfo(new ChartRenderingInfo())), dataArea, plot, plot.getDomainAxis(), plot.getRangeAxis(), dataset, 0, 0, false, 1);
    	
    	// make sure something is written
    	assertFalse(TestUtilities.bufferedImagesEqual(bufferedImage, cloneBufferedImage));
    }
    
    /**
     * Checks if drawing the nodes is done correctly
     */
    public void testTimeSeriesDrawShapeImageVertical() {
    	BufferedImage bufferedImage = new BufferedImage(200, 200, BufferedImage.TYPE_INT_RGB);
    	Graphics2D g2 = bufferedImage.createGraphics();
    	TimeSeriesCollection dataset = new TimeSeriesCollection();
    	dataset.addSeries(new TimeSeries("NaNTest"));
      	dataset.getSeries(0).add(new Month(2, 2001), 181.8);
      	dataset.getItem(0, 0).setShapeImage("resources/pokeball.png");
    	dataset.getSeries(0).fireSeriesChanged();
  
    	
    	JFreeChart chart = ChartFactory.createTimeSeriesChart("Test Chart", "X", "Y", dataset, false);
    	XYPlot plot = (XYPlot) chart.getPlot();
    	plot.setOrientation(PlotOrientation.VERTICAL);
    	XYLineAndShapeRenderer renderer = (XYLineAndShapeRenderer)plot.getRenderer();
    	
    	
    	Rectangle2D dataArea = new Rectangle2D.Double(0, 0, 200, 200);
    	
    	BufferedImage cloneBufferedImage = new BufferedImage(200, 200, BufferedImage.TYPE_INT_RGB);
    	
    	renderer.drawItem(g2, new XYItemRendererState(new PlotRenderingInfo(new ChartRenderingInfo())), dataArea, plot, plot.getDomainAxis(), plot.getRangeAxis(), dataset, 0, 0, false, 1);
    	
    	// make sure something is written
    	assertFalse(TestUtilities.bufferedImagesEqual(bufferedImage, cloneBufferedImage));
    }
    
    /**
     * Checks if drawing the nodes is done correctly
     */
    public void testTimeSeriesDrawShape2UseFillpaint() {
    	BufferedImage bufferedImage = new BufferedImage(200, 200, BufferedImage.TYPE_INT_RGB);
    	Graphics2D g2 = bufferedImage.createGraphics();
    	TimeSeriesCollection dataset = new TimeSeriesCollection();
    	dataset.addSeries(new TimeSeries("NaNTest"));
      	dataset.getSeries(0).add(new Month(2, 2001), 181.8);
      	dataset.getItem(0, 0).setUseFillPaint(true);
    	dataset.getSeries(0).fireSeriesChanged();
    	
    	
    	JFreeChart chart = ChartFactory.createTimeSeriesChart("Test Chart", "X", "Y", dataset, false);
    	XYPlot plot = (XYPlot) chart.getPlot();
    	plot.setOrientation(PlotOrientation.HORIZONTAL);
    	XYLineAndShapeRenderer renderer = (XYLineAndShapeRenderer)plot.getRenderer();
    	renderer.setSeriesShapesVisible(0, true);
    	Rectangle2D dataArea = new Rectangle2D.Double(0, 0, 200, 200);
    	
    	BufferedImage cloneBufferedImage = new BufferedImage(200, 200, BufferedImage.TYPE_INT_RGB);
    	
    	renderer.drawItem(g2, new XYItemRendererState(new PlotRenderingInfo(new ChartRenderingInfo())), dataArea, plot, plot.getDomainAxis(), plot.getRangeAxis(), dataset, 0, 0, false, 1);
    	
    	// make sure something is written
    	assertFalse(TestUtilities.bufferedImagesEqual(bufferedImage, cloneBufferedImage));
    }
    
    /**
     * Checks if drawing the nodes is done correctly
     */
    public void testTimeSeriesDrawShape2UseOutlinePaint() {
    	BufferedImage bufferedImage = new BufferedImage(200, 200, BufferedImage.TYPE_INT_RGB);
    	Graphics2D g2 = bufferedImage.createGraphics();
    	TimeSeriesCollection dataset = new TimeSeriesCollection();
    	dataset.addSeries(new TimeSeries("NaNTest"));
      	dataset.getSeries(0).add(new Month(2, 2001), 181.8);
      	dataset.getItem(0, 0).setUseOutlinePaint(true);
    	dataset.getSeries(0).fireSeriesChanged();
  
    	
    	JFreeChart chart = ChartFactory.createTimeSeriesChart("Test Chart", "X", "Y", dataset, false);
    	XYPlot plot = (XYPlot) chart.getPlot();
    	plot.setOrientation(PlotOrientation.HORIZONTAL);
    	XYLineAndShapeRenderer renderer = (XYLineAndShapeRenderer)plot.getRenderer();
    	renderer.setSeriesShapesVisible(0, true);
    	Rectangle2D dataArea = new Rectangle2D.Double(0, 0, 200, 200);
    	
    	BufferedImage cloneBufferedImage = new BufferedImage(200, 200, BufferedImage.TYPE_INT_RGB);
    	
    	renderer.drawItem(g2, new XYItemRendererState(new PlotRenderingInfo(new ChartRenderingInfo())), dataArea, plot, plot.getDomainAxis(), plot.getRangeAxis(), dataset, 0, 0, false, 1);
    	
    	// make sure something is written
    	assertFalse(TestUtilities.bufferedImagesEqual(bufferedImage, cloneBufferedImage));
    }
    
    /**
     * Checks if drawing the nodes is done correctly
     */
    public void testTimeSeriesDrawShape2Label() {
    	BufferedImage bufferedImage = new BufferedImage(200, 200, BufferedImage.TYPE_INT_RGB);
    	Graphics2D g2 = bufferedImage.createGraphics();
    	TimeSeriesCollection dataset = new TimeSeriesCollection();
    	dataset.addSeries(new TimeSeries("NaNTest"));
      	dataset.getSeries(0).add(new Month(2, 2001), 181.8);
      	dataset.getItem(0, 0).setDrawLabel(true);
    	dataset.getSeries(0).fireSeriesChanged();
  
    	
    	JFreeChart chart = ChartFactory.createTimeSeriesChart("Test Chart", "X", "Y", dataset, false);
    	XYPlot plot = (XYPlot) chart.getPlot();
    	plot.setOrientation(PlotOrientation.HORIZONTAL);
    	XYLineAndShapeRenderer renderer = (XYLineAndShapeRenderer)plot.getRenderer();
    	renderer.setSeriesShapesVisible(0, true);
    	Rectangle2D dataArea = new Rectangle2D.Double(0, 0, 200, 200);
    	
    	BufferedImage cloneBufferedImage = new BufferedImage(200, 200, BufferedImage.TYPE_INT_RGB);
    	
    	renderer.drawItem(g2, new XYItemRendererState(new PlotRenderingInfo(new ChartRenderingInfo())), dataArea, plot, plot.getDomainAxis(), plot.getRangeAxis(), dataset, 0, 0, false, 1);
    	
    	// make sure something is written
    	assertFalse(TestUtilities.bufferedImagesEqual(bufferedImage, cloneBufferedImage));
    }
    
    /**
     * Checks if drawing the nodes is done correctly
     */
    public void testTimeSeriesDrawShape2Outline() {
    	BufferedImage bufferedImage = new BufferedImage(200, 200, BufferedImage.TYPE_INT_RGB);
    	Graphics2D g2 = bufferedImage.createGraphics();
    	TimeSeriesCollection dataset = new TimeSeriesCollection();
    	dataset.addSeries(new TimeSeries("NaNTest"));
      	dataset.getSeries(0).add(new Month(2, 2001), 181.8);
      	dataset.getItem(0, 0).setDrawOutline(true);
      	dataset.getItem(0, 0).setOutlineStroke(new BasicStroke(1.0f));
    	dataset.getSeries(0).fireSeriesChanged();
  
    	
    	JFreeChart chart = ChartFactory.createTimeSeriesChart("Test Chart", "X", "Y", dataset, false);
    	XYPlot plot = (XYPlot) chart.getPlot();
    	plot.setOrientation(PlotOrientation.HORIZONTAL);
    	XYLineAndShapeRenderer renderer = (XYLineAndShapeRenderer)plot.getRenderer();
    	renderer.setSeriesShapesVisible(0, true);
    	Rectangle2D dataArea = new Rectangle2D.Double(0, 0, 200, 200);
    	
    	BufferedImage cloneBufferedImage = new BufferedImage(200, 200, BufferedImage.TYPE_INT_RGB);
    	
    	renderer.drawItem(g2, new XYItemRendererState(new PlotRenderingInfo(new ChartRenderingInfo())), dataArea, plot, plot.getDomainAxis(), plot.getRangeAxis(), dataset, 0, 0, false, 1);
    	
    	// make sure something is written
    	assertFalse(TestUtilities.bufferedImagesEqual(bufferedImage, cloneBufferedImage));
    }
    
    /**
     * Checks if drawing the nodes is done correctly
     */
    public void testTimeSeriesDrawShape2OutlineWithPaint() {
    	BufferedImage bufferedImage = new BufferedImage(200, 200, BufferedImage.TYPE_INT_RGB);
    	Graphics2D g2 = bufferedImage.createGraphics();
    	TimeSeriesCollection dataset = new TimeSeriesCollection();
    	dataset.addSeries(new TimeSeries("NaNTest"));
      	dataset.getSeries(0).add(new Month(2, 2001), 181.8);
      	dataset.getItem(0, 0).setDrawOutline(true);
      	dataset.getItem(0, 0).setOutlineStroke(new BasicStroke(1.0f));
      	dataset.getItem(0, 0).setOutlinePaint(Color.GRAY);
      	dataset.getItem(0, 0).setUseOutlinePaint(true);
    	dataset.getSeries(0).fireSeriesChanged();
  
    	
    	JFreeChart chart = ChartFactory.createTimeSeriesChart("Test Chart", "X", "Y", dataset, false);
    	XYPlot plot = (XYPlot) chart.getPlot();
    	plot.setOrientation(PlotOrientation.HORIZONTAL);
    	XYLineAndShapeRenderer renderer = (XYLineAndShapeRenderer)plot.getRenderer();
    	renderer.setSeriesShapesVisible(0, true);
    	Rectangle2D dataArea = new Rectangle2D.Double(0, 0, 200, 200);
    	
    	BufferedImage cloneBufferedImage = new BufferedImage(200, 200, BufferedImage.TYPE_INT_RGB);
    	
    	renderer.drawItem(g2, new XYItemRendererState(new PlotRenderingInfo(new ChartRenderingInfo())), dataArea, plot, plot.getDomainAxis(), plot.getRangeAxis(), dataset, 0, 0, false, 1);
    	
    	// make sure something is written
    	assertFalse(TestUtilities.bufferedImagesEqual(bufferedImage, cloneBufferedImage));
    }
    
    /**
     * Checks if drawing the nodes is done correctly
     */
    public void testTimeSeriesDrawShape2NaN() {
    	BufferedImage bufferedImage = new BufferedImage(200, 200, BufferedImage.TYPE_INT_RGB);
    	Graphics2D g2 = bufferedImage.createGraphics();
    	TimeSeriesCollection dataset = new TimeSeriesCollection();
    	dataset.addSeries(new TimeSeries("NaNTest"));
      	dataset.getSeries(0).add(new Month(2, 2001), Double.NaN);
    	dataset.getSeries(0).fireSeriesChanged();
  
    	
    	JFreeChart chart = ChartFactory.createTimeSeriesChart("Test Chart", "X", "Y", dataset, false);
    	XYPlot plot = (XYPlot) chart.getPlot();
    	plot.setOrientation(PlotOrientation.HORIZONTAL);
    	XYLineAndShapeRenderer renderer = (XYLineAndShapeRenderer)plot.getRenderer();
    	renderer.setSeriesShapesVisible(0, true);
    	Rectangle2D dataArea = new Rectangle2D.Double(0, 0, 200, 200);
    	
    	BufferedImage cloneBufferedImage = new BufferedImage(200, 200, BufferedImage.TYPE_INT_RGB);
    	
    	renderer.drawItem(g2, new XYItemRendererState(new PlotRenderingInfo(new ChartRenderingInfo())), dataArea, plot, plot.getDomainAxis(), plot.getRangeAxis(), dataset, 0, 0, false, 1);
    	
    	// make sure that nothing is written
    	assertTrue(TestUtilities.bufferedImagesEqual(bufferedImage, cloneBufferedImage));
    }

    /**
     * Check that the renderer is calculating the range bounds correctly.
     */
    public void testFindRangeBounds() {
        TableXYDataset dataset
                = RendererXYPackageTests.createTestTableXYDataset();
        JFreeChart chart = ChartFactory.createXYLineChart("Test Chart", 
                "X", "Y", dataset, false);
        XYPlot plot = (XYPlot) chart.getPlot();
        NumberAxis rangeAxis = (NumberAxis) plot.getRangeAxis();
        rangeAxis.setAutoRangeIncludesZero(false);
        Range bounds = rangeAxis.getRange();
        assertFalse(bounds.contains(1.0));
        assertTrue(bounds.contains(2.0));
        assertTrue(bounds.contains(5.0));
        assertFalse(bounds.contains(6.0));
    }

    /**
     * A check for the datasetIndex and seriesIndex fields in the LegendItem
     * returned by the getLegendItem() method.
     */
    public void testGetLegendItemSeriesIndex() {
        XYSeriesCollection d1 = new XYSeriesCollection();
        XYSeries s1 = new XYSeries("S1");
        s1.add(1.0, 1.1);
        XYSeries s2 = new XYSeries("S2");
        s2.add(1.0, 1.1);
        d1.addSeries(s1);
        d1.addSeries(s2);

        XYSeriesCollection d2 = new XYSeriesCollection();
        XYSeries s3 = new XYSeries("S3");
        s3.add(1.0, 1.1);
        XYSeries s4 = new XYSeries("S4");
        s4.add(1.0, 1.1);
        XYSeries s5 = new XYSeries("S5");
        s5.add(1.0, 1.1);
        d2.addSeries(s3);
        d2.addSeries(s4);
        d2.addSeries(s5);

        XYLineAndShapeRenderer r = new XYLineAndShapeRenderer();
        XYPlot plot = new XYPlot(d1, new NumberAxis("x"),
                new NumberAxis("y"), r);
        plot.setDataset(1, d2);
        /*JFreeChart chart =*/ new JFreeChart(plot);
        LegendItem li = r.getLegendItem(1, 2);
        assertEquals("S5", li.getLabel());
        assertEquals(1, li.getDatasetIndex());
        assertEquals(2, li.getSeriesIndex());
    }
    
    /**
     * Tests if all properties of a LegendItem are properly set
     */
    public void testLegendItem() {
        XYSeriesCollection d1 = new XYSeriesCollection();
        XYSeries s1 = new XYSeries("S1");
        s1.add(1.0, 1.1);
        XYSeries s2 = new XYSeries("S2");
        s2.add(1.0, 1.1);
        d1.addSeries(s1);
        d1.addSeries(s2);

        XYSeriesCollection d2 = new XYSeriesCollection();
        XYSeries s3 = new XYSeries("S3");
        s3.add(1.0, 1.1);
        XYSeries s4 = new XYSeries("S4");
        s4.add(1.0, 1.1);
        XYSeries s5 = new XYSeries("S5");
        s5.add(1.0, 1.1);
        d2.addSeries(s3);
        d2.addSeries(s4);
        d2.addSeries(s5);

        XYLineAndShapeRenderer r = new XYLineAndShapeRenderer();
        XYPlot plot = new XYPlot(d1, new NumberAxis("x"),
                new NumberAxis("y"), r);
        plot.setDataset(1, d2);
        /*JFreeChart chart =*/ new JFreeChart(plot);
        LegendItem li = r.getLegendItem(1, 2);
        
        assertEquals(li.isShapeVisible(), r.getItemShapeVisible(2, 0));
        assertEquals(li.isShapeFilled(), r.getItemShapeFilled(2, 0));
        
        // first test for use fill paint true
        r.setUseFillPaint(true);
        li = r.getLegendItem(1, 2);
        assertEquals(li.getFillPaint(), r.lookupSeriesFillPaint(2));
        
        // now test for use fill paint false
        r.setUseFillPaint(false);
        li = r.getLegendItem(1, 2);
        assertEquals(li.getFillPaint(), r.lookupSeriesPaint(2));
        assertEquals(li.isShapeOutlineVisible(), r.getDrawOutlines());
        
        // use outline true
        r.setUseOutlinePaint(true);
        li = r.getLegendItem(1, 2);
        assertEquals(li.getOutlinePaint(), r.lookupSeriesOutlinePaint(2));
        
        // use outline false
        r.setUseOutlinePaint(false);
        li = r.getLegendItem(1, 2);
        assertEquals(li.getOutlinePaint(), r.lookupSeriesPaint(2));
        
   
        assertEquals(li.isLineVisible(), r.getItemLineVisible(2, 0));
        assertEquals(li.getLineStroke(), r.lookupSeriesStroke(2));
        assertEquals(li.getLinePaint(), r.lookupSeriesPaint(2));
        assertEquals(li.getLine(), r.getLegendLine());
    }

}
