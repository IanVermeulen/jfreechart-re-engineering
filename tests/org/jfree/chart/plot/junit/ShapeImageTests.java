package org.jfree.chart.plot.junit;

import java.awt.Graphics2D;
import java.awt.Shape;
import java.awt.geom.Rectangle2D;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;

import org.jfree.chart.junit.TestUtilities;
import org.jfree.chart.plot.ShapeImage;
import org.jfree.chart.util.junit.ShapeUtilitiesTests;

import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;

public class ShapeImageTests extends TestCase {
    /**
     * Returns the tests as a test suite.
     *
     * @return The test suite.
     */
    public static Test suite() {
        return new TestSuite(ShapeImageTests.class);
    }
    
    /**
     * Tests the constructor with valid arguments
     */
    public void testConstructor() {
    	// construct a proper new shape image
    	ShapeImage shapeImage = new ShapeImage("resources/pokeball.png");
    	
    	// make sure that all getters return nonzero value
    	assertNotSame(shapeImage.getImage(), null);
    	assertNotSame(shapeImage.getBounds(), null);
    	assertNotSame(shapeImage.getHotspot(), null);
    }
    
    /**
     * Tests load from file
     */
    public void testLoadFromFile() {
    	// load an image the normal way
    	ShapeImage shapeImage = new ShapeImage("resources/pokeball.png");
    	
    	try {
			shapeImage.loadFromFile("resources/greatball.png");
		} catch (Exception e1) {
			e1.printStackTrace();
		}
    	
    	assertNotSame(shapeImage.getImage(), null);
    	
    	BufferedImage image = null;
    	try {
			 image = ImageIO.read(new File("resources/greatball.png"));
		} catch (IOException e) {
			e.printStackTrace();
		}
    	
    	assertNotSame(image, null);
    	assertTrue(TestUtilities.bufferedImagesEqual(shapeImage.getImage(), image));
    }
    
    /**
     * Tests load image errors
     */
    public void testExceptionLoadFromFile() {
    	// create a new shape image (pokeball as example)
    	ShapeImage shapeImage = new ShapeImage("resources/pokeball.png");
    	
    	// flag to check if tests succeeded, default is false
    	// the exception sets this to true to indicate that an exception was thrown
    	boolean passed = false;
    	
    	try {
    		// load it from a non-existing file
    		shapeImage.loadFromFile("DOESNOTEXIST");
    	} catch(Exception e) {
    		// so the error should be thrown
    		passed = true;
    	}
    	
    	assertTrue(passed);
    }
    
    /**
     * Tests the drawing of the shapeImage
     */
    public void testDraw() {
    	// create two empty images
    	BufferedImage image = new BufferedImage(50, 50, BufferedImage.TYPE_INT_RGB);
    	BufferedImage emptyImage = new BufferedImage(50, 50, BufferedImage.TYPE_INT_RGB);
    	
    	ShapeImage shapeImage = new ShapeImage("resources/pokeball.png");
    	
    	// draw the shape image on the empty image
    	shapeImage.draw((Graphics2D)image.getGraphics());
    	
    	// make sure that image is non-empty now
    	assertFalse(TestUtilities.bufferedImagesEqual(image, emptyImage));
    }

    /**
     * Tests the translation of a shape image with step (0, 0)
     */
    public void testZeroTranslation() {
    	ShapeImage shapeImage = new ShapeImage("resources/pokeball.png");
    	Rectangle2D originalBounds = shapeImage.getBounds();
    	
    	shapeImage.translate(0, 0);
    	
    	Rectangle2D newBounds = shapeImage.getBounds();
    	
    	assertTrue(originalBounds.equals(newBounds));
    }
    
    /**
     * Tests the translation of a shape image with step (1, 1)
     */
    public void testTranslation() {
    	ShapeImage shapeImage = new ShapeImage("resources/pokeball.png");
    	Rectangle2D originalBounds = shapeImage.getBounds();
    	
    	shapeImage.translate(1, 1);
    	
    	Rectangle2D newBounds = shapeImage.getBounds();
    	
    	assertFalse(originalBounds.equals(newBounds));
    	assertEquals(newBounds.getX(), 1.0);
    	assertEquals(newBounds.getY(), 1.0);
    	assertEquals(newBounds.getWidth(), originalBounds.getWidth());
    	assertEquals(newBounds.getHeight(), originalBounds.getHeight());
    }
    
    /**
     * Tests if the hotspot of the image is correct
     */
    public void testHotspot() {
    	ShapeImage shapeImage = new ShapeImage("resources/pokeball.png");
    	Rectangle2D originalBounds = shapeImage.getBounds();
    	
    	assertNotNull(originalBounds);
    	assertSame((Shape)originalBounds, shapeImage.getHotspot());
    }
}
