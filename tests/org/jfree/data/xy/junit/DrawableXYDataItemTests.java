package org.jfree.data.xy.junit;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.geom.Rectangle2D;
import java.awt.geom.Rectangle2D.Double;
import java.awt.image.BufferedImage;

import org.jfree.chart.junit.TestUtilities;
import org.jfree.data.time.Month;
import org.jfree.data.time.TimeSeries;
import org.jfree.data.time.TimeSeriesCollection;
import org.jfree.data.xy.DrawableXYDataItem;

import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;

public class DrawableXYDataItemTests extends TestCase {
	/**
     * Returns the tests as a test suite.
     *
     * @return The test suite.
     */
    public static Test suite() {
        return new TestSuite(DrawableXYDataItemTests.class);
    }
    
    /**
     * Tests the setters and getters
     */
    public void testSettersAndGetters() {
    	DrawableXYDataItem dataItem = new DrawableXYDataItem();
    	
    	// draw label flag
    	dataItem.setDrawLabel(true);
    	assertEquals(dataItem.shouldDrawLabel(), true);
    	dataItem.setDrawLabel(false);
    	assertEquals(dataItem.shouldDrawLabel(), false);
    	
    	// draw outline flag
    	dataItem.setDrawOutline(true);
    	assertEquals(dataItem.shouldDrawOutline(), true);
    	dataItem.setDrawOutline(false);
    	assertEquals(dataItem.shouldDrawOutline(), false);
    	
    	// filled flag
    	dataItem.setFilled(true);
    	assertEquals(dataItem.isFilled(), true);
    	dataItem.setFilled(false);
    	assertEquals(dataItem.isFilled(), false);
    	
    	// fill paint
    	dataItem.setFillPaint(Color.decode("0x0F0F81"));
    	assertEquals(dataItem.getFillPaint(), Color.decode("0x0F0F81"));
    	
       	// set selected
    	dataItem.setSelected(true);
    	assertEquals(dataItem.isSelected(), true);
    	dataItem.setSelected(false);
    	assertEquals(dataItem.isSelected(), false);
    	
    	// outline paint
    	dataItem.setOutlinePaint(Color.decode("0x0F0F81"));
    	dataItem.setOutlineSelectedPaint(Color.decode("0x0F0F82"));
    	assertEquals(dataItem.getOutlinePaint(), Color.decode("0x0F0F81"));
    	dataItem.setSelected(true);
    	assertEquals(dataItem.getOutlinePaint(), Color.decode("0x0F0F82"));
    	dataItem.setSelected(false);
    	
    	// paint
    	dataItem.setPaint(Color.decode("0x0F0F81"));
    	assertEquals(dataItem.getPaint(), Color.decode("0x0F0F81"));
    	
    	// shape
    	dataItem.setShape(new Rectangle2D.Double(0, 0, 5.0, 5.0));
    	dataItem.setShapeSelected(new Rectangle2D.Double(0, 0, 10.0, 10.0));
    	assertTrue(dataItem.getShape().equals(new Rectangle2D.Double(0, 0, 5.0, 5.0)));
    	dataItem.setSelected(true);
    	assertTrue(dataItem.getShape().equals(new Rectangle2D.Double(0, 0, 10.0, 10.0)));
    	dataItem.setSelected(false);
    
    	// shape image
    	dataItem.setShapeImage("resources/pokeball.png");
    	assertTrue(dataItem.shouldDrawShapeImage());
    	
    	// use fill paint
    	dataItem.setUseFillPaint(true);
    	assertEquals(dataItem.shouldUseFillPaint(), true);
    	dataItem.setUseFillPaint(false);
    	assertEquals(dataItem.shouldUseFillPaint(), false);
    	
    	// use outline paint
    	dataItem.setUseOutlinePaint(true);
    	assertEquals(dataItem.shouldUseOutlinePaint(), true);
    	dataItem.setUseOutlinePaint(false);
    	assertEquals(dataItem.shouldUseOutlinePaint(), false);
    	
    	// is visible
    	dataItem.setVisible(true);
    	assertEquals(dataItem.isVisible(), true);
    	dataItem.setVisible(false);
    	assertEquals(dataItem.isVisible(), false);

    	// outline stroke
    	dataItem.setOutlineStroke(new BasicStroke(1.0f));
    	assertTrue(dataItem.getOutlineStroke().equals(new BasicStroke(1.0f)));
    }
    
    /**
     * Tests the drawShape2 method
     */
    public void testDrawShape2() {
    	BufferedImage image = new BufferedImage(50, 50, BufferedImage.TYPE_INT_RGB);
    	BufferedImage cloneImage = new BufferedImage(50, 50, BufferedImage.TYPE_INT_RGB);
    	TimeSeriesCollection dataset = new TimeSeriesCollection();
    	dataset.addSeries(new TimeSeries("NaNTest"));
      	dataset.getSeries(0).add(new Month(2, 2001), 181.8);
    	dataset.getSeries(0).fireSeriesChanged();
    	
    	DrawableXYDataItem dataItem = new DrawableXYDataItem();
//    	dataItem.drawShape2((Graphics2D)image.getGraphics(), new Rectangle2D.Double(0, 0, 50, 50), plot, dataset, pass, series, item, selected, domainAxis, rangeAxis, crosshairState, entities, renderer);
    }
    
}
